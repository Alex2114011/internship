# Internship

День первый
📖Материалы к Занятию 1. Вводное данятие
 
 [Презентация](https://bitbucket.org/Alex2114011/internship/src/master/Lesson%E2%84%961/1%20%D0%B4%D0%B5%D0%BD%D1%8C_iOS.pdf)
 
 1. https://habr.com/ru/post/451130/ - must ✅
 2. https://habr.com/ru/company/vivid_money/blog/592599/ - medium
 3. https://www.youtube.com/watch?v=ERYNyrfXjlg - Mike Ash (ultra hard)
 4. https://github.com/apple/swift/blob/swift-5.5-RELEASE/stdlib/public/SwiftShims/RefCount.h - Apple/Swift про Reference Count - HARD
 5. https://www.objc.io/books/advanced-swift/ - хорошая книга, местами HARD
 6. https://www.swift.org - грааль!
 
 [ДЗ](https://bitbucket.org/Alex2114011/internship/src/master/Lesson%E2%84%961/%D0%94%D0%97/)


День второй

 [Презентация](https://bitbucket.org/Alex2114011/internship/src/master/Lesson%E2%84%962/%D0%9F%D1%80%D0%B5%D0%B7%D1%8B%20%D0%B2%20%D1%80%D0%B0%D0%B7%D0%BD%D1%8B%D1%85%20%D1%84%D0%BE%D1%80%D0%BC%D0%B0%D1%82%D0%B0%D1%85/)
 
 1. Head First (книга) - базовое понимание ООП и потерны - medium 
 2. https://habr.com/ru/company/acronis/blog/420239/ - medium 
 3. https://medium.com/@bakshioye/static-vs-dynamic-dispatch-in-swift-a-decisive-choice-cece1e872d - тут ответы которые, к вопросам которые будут после первой статьи  hard
 4. https://www.objc.io/books/advanced-swift/ - протаколы, коллекции - hard 
 5. https://developer.apple.com/videos/play/wwdc2015/408/ - hard 
 6. https://youtu.be/ZbqgV8yVv4E - hard
 7. [Пример паттерна делегирование](https://bitbucket.org/Alex2114011/internship/src/master/Lesson%E2%84%962/CourseDelegateProtocols/)

[ДЗ](https://bitbucket.org/Alex2114011/internship/src/master/Lesson%E2%84%962/%D0%94%D0%97/) 


День третий
 [Презентация](https://bitbucket.org/Alex2114011/internship/src/master/Lesson%E2%84%963/03.%20SOLID%2C%20DRY%2C%20KISS.pdf)
 
  1. https://habr.com/ru/company/itelma/blog/546372/
  2. Роберт Мартин - чистая архитектура

[ДЗ](https://bitbucket.org/Alex2114011/internship/src/master/Lesson%E2%84%963/%D0%94%D0%97/)

День четвертый

[Презентация](https://bitbucket.org/Alex2114011/internship/src/master/Lesson%E2%84%964/Architectures.pptx)

[Examples](https://bitbucket.org/Alex2114011/internship/src/master/Lesson%E2%84%964/) 


День пятый

[Презентация](https://bitbucket.org/Alex2114011/internship/src/master/Lesson%E2%84%964/Architectures.pptx)

[ДЗ](https://bitbucket.org/Alex2114011/internship/src/master/Lesson%E2%84%965/Validation/)



