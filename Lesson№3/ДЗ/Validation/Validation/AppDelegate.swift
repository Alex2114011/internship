//
//  AppDelegate.swift
//  Validation
//
//  Created by Alex on 22.02.2022.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        let window = UIWindow(frame: UIScreen.main.bounds)
        let rootVC = ValidationScreenViewController()
        window.rootViewController = rootVC
        self.window = window
        window.makeKeyAndVisible()
        return true
    }
}

