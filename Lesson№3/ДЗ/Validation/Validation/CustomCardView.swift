//
//  CustomCardView.swift
//  Validation
//
//  Created by Alex on 24.02.2022.
//

import UIKit

final class CustomCardView: UIView {

    //MARK: - Public property
    var textCallback: ((String) -> ())?

    //MARK: - Private property
    private lazy var textField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.borderStyle = .roundedRect
        textField.backgroundColor = .systemBackground
        return textField
    }()

    private lazy var button: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .systemBlue
        button.layer.cornerRadius = 8
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 17, weight: .bold)
        button.addTarget(self, action: #selector(didTapValidateButton), for: .touchUpInside)
        return button
    }()

    private lazy var label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 17, weight: .medium)
        return label
    }()

    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    convenience init(frame: CGRect,
                     textFieldPlaceholder: String,
                     buttonTapTitle: String,
                     buttonHoldTapTitle: String,
                     labelText: String) {
        self.init(frame: frame)
        self.textField.placeholder = textFieldPlaceholder
        self.button.setTitle(buttonTapTitle, for: .normal)
        self.button.setTitle(buttonHoldTapTitle, for: .highlighted)
        self.label.text = labelText
        configureView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    // MARK: - Private methods
    private func configureView() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = .lightGray

        let layer = self.layer
        layer.cornerRadius = 8
        layer.shadowOffset = CGSize(width: 5, height:6)
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 5

        self.addSubview(textField)
        self.addSubview(button)
        self.addSubview(label)

        NSLayoutConstraint.activate([
            textField.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
            textField.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 8),
            textField.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -8),

            button.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: 10),
            button.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 8),
            button.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -8),

            label.topAnchor.constraint(equalTo: button.bottomAnchor, constant: 10),
            label.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 8),
            label.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -8),
            label.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -16)
        ])
    }

    @objc private func didTapValidateButton() {
        guard let text = textField.text else { return }
        textCallback?(text)
    }


    // MARK: - Public methods
    func changeStateLabel(isTextValid: Bool) {
        if isTextValid {
            self.label.text = "Success"
            self.label.textColor = UIColor.white
        } else {
            self.label.text = "Fail try again"
            self.label.textColor = UIColor.red
            animateError()
        }
    }

    
    // MARK: - Animations
    private func animateError() {
        UIView.animate(withDuration: 1.5,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 0,
                       options: .curveEaseIn) {
            self.textField.bounds.size.width += 50.0
        } completion: { _ in
        }
    }
}
