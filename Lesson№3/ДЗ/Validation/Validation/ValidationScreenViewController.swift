//
//  ValidationScreen.swift
//  Validation
//
//  Created by Alex on 24.02.2022.
//

import UIKit

final class ValidationScreenViewController: UIViewController {

    //MARK: - Private property
    private lazy var nameCardView = CustomCardView(frame: .zero,
                                                   textFieldPlaceholder: "Enter full name...",
                                                   buttonTapTitle: "Validate name",
                                                   buttonHoldTapTitle: "don't push me!",
                                                   labelText: "Wait validation")

    private lazy var emailCardView = CustomCardView(frame: .zero,
                                                    textFieldPlaceholder: "Enter email...",
                                                    buttonTapTitle: "Validate email",
                                                    buttonHoldTapTitle: "You hold me!",
                                                    labelText: "Wait validation")

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        validateText()
    }
}


//MARK: - Private Methods
extension ValidationScreenViewController {
    private func configureView() {
        view.backgroundColor = .systemBackground
        view.addSubview(nameCardView)
        view.addSubview(emailCardView)
        
        NSLayoutConstraint.activate([
            nameCardView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            nameCardView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            nameCardView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),

            emailCardView.topAnchor.constraint(equalTo: nameCardView.bottomAnchor, constant: 30),
            emailCardView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            emailCardView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20)
        ])
    }

    private func validateText() {
         nameCardView.textCallback = { [weak self] text in
             guard let self = self else { return }
             let regExString = "[A-Za-zА-ЯЁа-яё-]{2,}+\\s{1}+[A-Za-zА-ЯЁа-яё-]{2,32}"
             let predicate = NSPredicate(format: "SELF MATCHES[c] %@", regExString)
             let isValid = predicate.evaluate(with: text)
             self.nameCardView.changeStateLabel(isTextValid: isValid)
         }

         emailCardView.textCallback = { [weak self] text in
             guard let self = self else { return }
             let regExString = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
             let predicate = NSPredicate(format: "SELF MATCHES %@", regExString)
             let isValid = predicate.evaluate(with: text)
             self.emailCardView.changeStateLabel(isTextValid: isValid)
         }
     }
}
