//
//  NutrientCalculator.swift
//  forTest
//
//  Created by Alex on 26.02.2022.
//

import UIKit

enum ProportionType {
    case paleo
    case keto
}

enum CaloriesCalculationType {
    case mifflin
    case harrisBenedict
}

struct Nutrients {
    let carbs: Int
    let fats: Int
    let proteins: Int
}

protocol NutrientCalculator {
    func calculateDailtyNutrients(proportionType: ProportionType,
                                  caloriesCalculatorType: CaloriesCalculationType) -> Nutrients
}

struct NutrientCalculatorImpl: NutrientCalculator {

    func calculateDailtyNutrients(proportionType: ProportionType,
                                  caloriesCalculatorType: CaloriesCalculationType) -> Nutrients {
        var targetCalories: Int
        switch caloriesCalculatorType {
        case .mifflin:
            // some calculation
            targetCalories = 1200
        case .harrisBenedict:
            // some calculation
            targetCalories = 1400
        }
        var nutrients: Nutrients
        switch proportionType {
        case .paleo:
            // use calories to calculate
            // nutrients
            nutrients = Nutrients(carbs: 4, fats: 10, proteins: 8)
        case .keto:
            // use calories to calculate
            // nutrients
            nutrients = Nutrients(carbs: 6, fats: 1, proteins: 20)
        }
        return nutrients
    }
}


final class SRPViewController: UIViewController {

   private var nutrientsCalculator: NutrientCalculator

    init(nutrientsCalculator: NutrientCalculator) {
        self.nutrientsCalculator = nutrientsCalculator
        super.init(nibName: nil, bundle: nil)
    }


    override func viewDidLoad() {
        super.viewDidLoad()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
