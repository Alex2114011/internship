import UIKit

// Задание №1
// Написать код, где можно будет сравнивать перечисления.
// Где перечисления могут быть =>, >, <=, < исходя их rating и их названия самого перечисления


//enum LessonType {
//    case math(rating: Int)
//    case nativeLanguage(rating: Int)
//    case computerSince(rating: Int)
//    case literature(rating: Int)
//}

// Задание №2
// Реализуйте протокол Equitable у LessonType





// Задание №1
enum LessonType {
    case math(rating: Int)
    case nativeLanguage(rating: Int)
    case computerSince(rating: Int)
    case literature(rating: Int)
}

extension LessonType {
    static func qualify(lhs: Self, rhs: Self) -> Bool {
        switch (lhs, rhs) {
        case (Self.math, Self.math):
            return true
        case (Self.nativeLanguage, Self.nativeLanguage):
            return true
        case (Self.computerSince, Self.computerSince):
            return true
        case (Self.literature, Self.literature):
            return true
        default:
            return false
        }
    }

    static func ratingValue(lhs: Self, rhs: Self) -> (Int, Int) {
        switch (lhs, rhs) {
        case (Self.math(let leftRating), Self.math(let rightRating)):
            return (leftRating, rightRating)
        case (Self.nativeLanguage(let leftRating), Self.nativeLanguage(let rightRating)):
            return (leftRating, rightRating)
        case (Self.computerSince(let leftRating), Self.computerSince(let rightRating)):
            return (leftRating, rightRating)
        case (Self.literature(let leftRating), Self.literature(let rightRating)):
            return (leftRating, rightRating)
        default: fatalError()
        }
    }


    static func > (lhs: Self, rhs: Self) -> Bool {
        if !qualify(lhs: lhs, rhs: rhs) {
            return false
        }
        let (leftValue, rightValue) = ratingValue(lhs: lhs, rhs: rhs)
        return leftValue > rightValue
    }

    static func < (lhs: Self, rhs: Self) -> Bool {
        if !qualify(lhs: lhs, rhs: rhs) {
            return false
        }
        let (leftValue, rightValue) = ratingValue(lhs: lhs, rhs: rhs)
        return leftValue < rightValue
    }

    static func >= (lhs: Self, rhs: Self) -> Bool {
        if !qualify(lhs: lhs, rhs: rhs) {
            return false
        }
        let (leftValue, rightValue) = ratingValue(lhs: lhs, rhs: rhs)
        return leftValue >= rightValue
    }

    static func <= (lhs: Self, rhs: Self) -> Bool {
        if !qualify(lhs: lhs, rhs: rhs) {
            return false
        }
        let (leftValue, rightValue) = ratingValue(lhs: lhs, rhs: rhs)
        return leftValue <= rightValue
    }
    // Задание №2
    static func == (lhs: Self, rhs: Self) -> Bool {
        if !qualify(lhs: lhs, rhs: rhs) {
            return false
        }
        let (leftValue, rightValue) = ratingValue(lhs: lhs, rhs: rhs)
        return leftValue == rightValue
    }
}

let equality = LessonType.math(rating: 6) == LessonType.math(rating: 6)

let larger = LessonType.math(rating: 6) > LessonType.math(rating: 6)
let less = LessonType.math(rating: 6) < LessonType.math(rating: 6)

let largerOrEquality = LessonType.math(rating: 6) >= LessonType.math(rating: 6)
let lessOrEquality = LessonType.math(rating: 6) <= LessonType.math(rating: 6)
