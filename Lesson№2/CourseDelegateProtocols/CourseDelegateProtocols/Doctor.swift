//
//  Doctor.swift
//  CourseDelegateProtocols
//
//  Created by Курганов Сергей Владимирович on 23.01.2022.
//

import Foundation

class Doctor {
}

extension Doctor {
    private func heal(_ patient: Patient) {
        if 37.0 <= patient.temperature && patient.temperature < 38.0 &&
            (patient.symptom == .cough || patient.symptom == .nausea) {
            patient.takePhill()
            
        } else if patient.temperature >= 38 || patient.symptom == .redThroat {
            patient.makeShort()
        } else {
            print("\(patient.name) should be rest")
        }
    }
}

extension Doctor: PatientDelegate {
    func feelWorse(_ patient: Patient) {
        heal(patient)
    }
}
