//
//  Patient.swift
//  CourseDelegateProtocols
//
//  Created by Курганов Сергей Владимирович on 23.01.2022.
//

import Foundation

 enum Symptom {
    case redThroat
    case cough
    case nausea
}

protocol PatientDelegate: AnyObject {
    func feelWorse(_ patient: Patient)
}

class Patient {
    let name: String
    let temperature: Float
    let symptom: Symptom
   
    weak var delegate: PatientDelegate?
    
    init(name: String, temperature: Float, symptom: Symptom) {
        self.name = name
        self.temperature = temperature
        self.symptom = symptom
    }
    
    func iFeelWorse() {
        delegate?.feelWorse(self)
    }
    
    func takePhill() {
        print("\(name) takes a phill")
    }
    
    func makeShort() {
        print("\(name) make a short")
    }
}
