//
//  ViewController.swift
//  CourseDelegateProtocols
//
//  Created by Курганов Сергей Владимирович on 23.01.2022.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let doctor = Doctor()
        
        let max = Patient(name: "Max", temperature: 37.1, symptom: .nausea)
        max.delegate = doctor
        
        let diana = Patient(name: "Diana", temperature: 39.1, symptom: .redThroat)
        diana.delegate = doctor
        
        let paul = Patient(name: "Paul", temperature: 36.6, symptom: .cough)
        paul.delegate = doctor
        
        let rob = Patient(name: "Rob", temperature: 36.9, symptom: .redThroat)
        rob.delegate = doctor
        
        let patients = [max, diana, paul, rob]
        for patient in patients {
            patient.iFeelWorse()
        }
    }
}

