//
//  ValidationNameView.swift
//  Validation
//
//  Created by Alex on 03.03.2022.
//

import UIKit

final class ValidationNameView: UIView {

    //MARK: - Public property

    lazy var nameCardView = CustomCardView(frame: .zero,
                                           textFieldPlaceholder: "Enter full name...",
                                           buttonTapTitle: "Validate name",
                                           buttonHoldTapTitle: "don't push me!",
                                           labelText: "Wait validation")

    //MARK: - Initialization

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK: - Private Methods
    
    private func configureView() {
        self.addSubview(nameCardView)
        
        NSLayoutConstraint.activate([
            nameCardView.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor,constant: 30),
            nameCardView.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            nameCardView.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -20)
        ])
    }
}
