//
//  ValidationEmailView.swift
//  Validation
//
//  Created by Alex on 02.03.2022.
//

import UIKit

final class ValidationEmailView: UIView {

    //MARK: - Public property

    lazy var emailCardView = CustomCardView(frame: .zero,
                                            textFieldPlaceholder: "Enter email...",
                                            buttonTapTitle: "Validate email",
                                            buttonHoldTapTitle: "You hold me!",
                                            labelText: "Wait validation")

    //MARK: - Initialization

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    //MARK: - Private Methods
    
    private func configureView() {
        self.addSubview(emailCardView)
        
        NSLayoutConstraint.activate([
            emailCardView.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 30),
            emailCardView.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            emailCardView.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -20)
        ])
    }
}
