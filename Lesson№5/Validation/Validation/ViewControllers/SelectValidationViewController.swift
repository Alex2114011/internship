//
//  SelectValidationViewController.swift
//  Validation
//
//  Created by Alex on 03.03.2022.
//

import UIKit

class SelectValidationViewController: UIViewController {

    //MARK: - Private Property

    private lazy var nameButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .systemBlue
        button.layer.cornerRadius = 8
        button.setTitleColor(.white, for: .normal)
        button.setTitle("Validate name", for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 17, weight: .bold)
        button.addTarget(self, action: #selector(didTapValidateNameButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var emailButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .systemBlue
        button.layer.cornerRadius = 8
        button.setTitleColor(.white, for: .normal)
        button.setTitle("Validate email", for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 17, weight: .bold)
        button.addTarget(self, action: #selector(didTapValidateEmailButton), for: .touchUpInside)
        return button
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
}

extension SelectValidationViewController {

    //MARK: - Private Methods

    private func configureUI() {
        view.backgroundColor = .white
        view.addSubview(nameButton)
        view.addSubview(emailButton)
        
        NSLayoutConstraint.activate([nameButton.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -20),
                                     nameButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                                     nameButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8),
                                     nameButton.heightAnchor.constraint(equalToConstant: 50),
                                     
                                     emailButton.topAnchor.constraint(equalTo: nameButton.bottomAnchor, constant: 20),
                                     emailButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                                     emailButton.heightAnchor.constraint(equalTo: nameButton.heightAnchor),
                                     emailButton.widthAnchor.constraint(equalTo: nameButton.widthAnchor)
                                    ])
    }
    
    @objc private func didTapValidateNameButton() {
        navigationController?.pushViewController(ValidationNameViewController(), animated: true)
    }
    
    @objc private func didTapValidateEmailButton() {
        navigationController?.present(ValidationEmailViewController(), animated: true, completion: nil)
    }
}

