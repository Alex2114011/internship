//
//  ValidationEmailViewController.swift
//  Validation
//
//  Created by Alex on 24.02.2022.
//

import UIKit

final class ValidationEmailViewController: UIViewController {
    
    override func loadView() {
        self.view = ValidationEmailView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        validateEmail()
    }
}

//MARK: - Private Methods

extension ValidationEmailViewController {
    private func configureView() {
        view.backgroundColor = .systemBackground
    }
    
    private func validateEmail() {
        guard let emailView = view as? ValidationEmailView else { return }
        emailView.emailCardView.textCallback = { text in
            let regExString = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
            let predicate = NSPredicate(format: "SELF MATCHES %@", regExString)
            let isValid = predicate.evaluate(with: text)
            emailView.emailCardView.changeStateLabel(isTextValid: isValid)
        }
    }
}
