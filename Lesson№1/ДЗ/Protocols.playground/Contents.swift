import UIKit

typealias Coffee = String


protocol CoffeeHouse {
    var barista: ManCoffeeMaker { get }
}


protocol ManCoffeeMaker {
    var filter: FilteredCoffee { get }
    var coffeeMaschine: AutomaticCoffeeMaker { get }
}


protocol FilteredCoffee {
    func makeFilteredCoffe() -> Coffee
}


protocol AutomaticCoffeeMaker {
    func makeCappucino() -> Coffee
}


class MyFavoriteCoffeeHouse: CoffeeHouse {

    var barista: ManCoffeeMaker

    init(barista: ManCoffeeMaker) {
        self.barista = barista
    }

}


class Barista: ManCoffeeMaker {

    var coffeeMaschine: AutomaticCoffeeMaker

    var filter: FilteredCoffee

    init(coffeeMaschine: AutomaticCoffeeMaker, filter: FilteredCoffee) {
        self.coffeeMaschine = coffeeMaschine
        self.filter = filter
    }
}



class Filter: FilteredCoffee {
    func makeFilteredCoffe() -> Coffee {
        return "Your filtered coffee please"
    }
}

class CoffeMaschine: AutomaticCoffeeMaker {
    func makeCappucino() -> Coffee {
        return "psss hhhh pop pluh - coffee done"
    }
}

class MySelf {

    var buyCoffee: CoffeeHouse

    init (buyCoffee: CoffeeHouse) {
        self.buyCoffee = buyCoffee
    }

}


let coffeeMaschine = CoffeMaschine()
let filter = Filter()
let barista = Barista(coffeeMaschine: coffeeMaschine, filter: filter)
let coffeeHouse = MyFavoriteCoffeeHouse(barista: barista)
let iAm = MySelf(buyCoffee: coffeeHouse)

iAm.buyCoffee.barista.coffeeMaschine.makeCappucino()
